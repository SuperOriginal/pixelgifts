package pw.soopr.pgifts;

import io.ibj.JLib.JPlug;
import io.ibj.JLib.file.ResourceFile;
import io.ibj.JLib.file.ResourceReloadHook;
import io.ibj.JLib.file.YAMLFile;
import io.ibj.JLib.utils.Colors;
import io.ibj.JLib.utils.ItemMetaFactory;
import io.puharesource.mc.titlemanager.api.TitleObject;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.inventory.ItemStack;

import java.util.*;

/**
 * Created by Aaron.
 */
public class PixelGifts extends JPlug implements ResourceReloadHook, Listener{

    private RandomCollection<ItemStack> items;
    private Set<RotationSession> sessions;
    private String spininv;
    public static PixelGifts i;
    private HashMap<ItemStack, List<String>> itemCommands;
    private ItemStack unchosen;
    private ItemStack chosen;
    private ItemStack chosenother;
    private YAMLFile config;

    private HashMap<UUID,Long> cd;

    @Override
    public void onModuleEnable(){
        i = this;
        cd = new HashMap<>();
        sessions = new HashSet<>();
        itemCommands = new HashMap<>();
        config = new YAMLFile(this,"config.yml");
        config.getConfig().options().copyDefaults(true);
        config.saveDefaultConfig();
        if(config.getConfig().getConfigurationSection("data") == null){
            config.getConfig().createSection("data");
            config.saveConfig();
        }
        for(String key : config.getConfig().getConfigurationSection("data").getKeys(false)){
            try{
                cd.put(UUID.fromString(key),config.getConfig().getLong("data." + key));
            }catch (IllegalArgumentException e){
                //none
            }
        }

        config.getConfig().set("data", null);
        registerResource(new YAMLFile(this, "config.yml", new ResourceReloadHook[]{this}));
        reload();
        registerEvents(this);
    }

    @Override
    public void onModuleDisable(){
        for(UUID key : cd.keySet()){
            config.getConfig().set("data." + key, cd.get(key));
        }
        config.saveConfig();
    }

    @Override
    public void onReload(ResourceFile file){
        items = new RandomCollection<>();
        for(String key : config.getConfig().getConfigurationSection("items").getKeys(false)){
            ConfigurationSection s = config.getConfig().getConfigurationSection("items." + key);
            ItemStack is = new ItemStack(Material.valueOf(s.getString("material")), s.getInt("amount"), (short)0, (byte)s.getInt("data"));
            ArrayList<String> lr = new ArrayList<>();
            s.getStringList("lore").forEach(st -> lr.add(Colors.colorify(st)));
            ItemMetaFactory.create(is).setDisplayName(Colors.colorify(s.getString("display"))).setLore(lr).set();
            items.add(s.getDouble("rarity"), is);
            itemCommands.put(is, s.getStringList("commands"));
        }
        {
            ConfigurationSection s = config.getConfig().getConfigurationSection("selector.unchosen");
            ItemStack is = new ItemStack(Material.valueOf(s.getString("material")), s.getInt("amount"), (short) 0, (byte) s.getInt("data"));
            ArrayList<String> lr = new ArrayList<>();
            s.getStringList("lore").forEach(st -> lr.add(Colors.colorify(st)));
            ItemMetaFactory.create(is).setDisplayName(Colors.colorify(s.getString("display"))).setLore(lr).set();
            unchosen = is;
        }

        {
            ConfigurationSection s = config.getConfig().getConfigurationSection("selector.chosen-other");
            ItemStack is = new ItemStack(Material.valueOf(s.getString("material")), s.getInt("amount"), (short) 0, (byte) s.getInt("data"));
            ArrayList<String> lr = new ArrayList<>();
            s.getStringList("lore").forEach(st -> lr.add(Colors.colorify(st)));
            ItemMetaFactory.create(is).setDisplayName(Colors.colorify(s.getString("display"))).setLore(lr).set();
            chosenother = is;
        }

        {
            ConfigurationSection s = config.getConfig().getConfigurationSection("selector.chosen");
        ItemStack is = new ItemStack(Material.valueOf(s.getString("material")), s.getInt("amount"), (short)0, (byte)s.getInt("data"));
            ArrayList<String> lr = new ArrayList<>();
            s.getStringList("lore").forEach(st -> lr.add(Colors.colorify(st)));
            ItemMetaFactory.create(is).setDisplayName(Colors.colorify(s.getString("display"))).setLore(lr).set();
        chosen = is;
        }

        spininv = Colors.colorify(config.getConfig().getString("spinning-inv"));
    }

    public boolean onCommand(CommandSender sender, Command cmd, String lbl, String[] args){
        if(cmd.getName().equalsIgnoreCase("daily")){
            if(!(sender instanceof Player)){
                sender.sendMessage("You are not allowed to do this fgt.");
                return true;
            }
            Player p = (Player) sender;
            if(!p.hasPermission("pixelgifts.use")){
                p.sendMessage(ChatColor.RED + "No permission.");
                return true;
            }
            if(cd.containsKey(p.getUniqueId())){
                if(cd.get(p.getUniqueId()) < System.currentTimeMillis()) {
                    cd.remove(p.getUniqueId());
                }else{
                    sendFloatingText(p,"",ChatColor.RED + "You can only use this command once every 24 hours.");
                    return true;
                }
            }
            if(p.getInventory().firstEmpty() == -1){
                sendFloatingText(p,ChatColor.RED + "Inventory Full!",ChatColor.RED + "Please clear an inventory slot before using this command.");
                return true;
            }
            new RotationSession(p);
        }
        return true;
    }

    @EventHandler
    public void onInitial(InventoryClickEvent e){
        if(e.getInventory().getName().equals(spininv)){
            if(e.getRawSlot() > 8 || e.getRawSlot() < 0) return;
            for(RotationSession session : sessions){
                if(session.getP().getName().equalsIgnoreCase(e.getWhoClicked().getName())){
                    if(session.locked) e.setCancelled(true);
                    if(!session.chosen){
                        session.rotate(e.getSlot());
                    }
                }
            }
        }
    }

    void sendFloatingText(Player player, String title, String subtitle) {
        TitleObject obj = new TitleObject(title, subtitle);
        obj.setFadeIn(20);
        obj.setStay(40);
        obj.setFadeOut(20);
        obj.send(player);
    }



    @EventHandler
    public void onClose(InventoryCloseEvent e){
            List<RotationSession> remove = new ArrayList<>();
            for(RotationSession session : sessions){
                if(e.getInventory().equals(session.getToRotate())){
                if(session.getP().getName().equals(e.getPlayer().getName())){
                    if(session.chosen){
                        if(session.getToRotate().getItem(session.getChosenSlot()) != null) {
                            Bukkit.getScheduler().cancelTask(session.getTask());
                            e.getPlayer().getInventory().addItem(session.getFinalItem());
                        }
                        cd.put(e.getPlayer().getUniqueId(),System.currentTimeMillis() + 86400000);
                    }else{
                        remove.add(session);
                    }
                }
            }
        }
        sessions.removeAll(remove);
    }

    public RandomCollection<ItemStack> getItems() {
        return items;
    }

    public Set<RotationSession> getSessions() {
        return sessions;
    }

    public String getSpininv() {
        return spininv;
    }

    public HashMap<ItemStack, List<String>> getItemCommands() {
        return itemCommands;
    }

    public ItemStack getUnchosen() {
        return unchosen;
    }

    public ItemStack getChosen() {
        return chosen;
    }

    public ItemStack getChosenother() {
        return chosenother;
    }
}
