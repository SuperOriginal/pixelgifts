package pw.soopr.pgifts;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.material.MaterialData;

/**
 * Created by Aaron.
 */
public class RotationSession {
    private Player p;
    private ItemStack finalItem;
    private ItemStack[] rotatingItems;
    private int counter = 0;
    private Inventory toRotate;
    private int task = 0;
    private int chosenSlot = -1;
    private RotationSession rotationSession;

    boolean locked;
    boolean chosen;

     public RotationSession(Player p){
         this.p = p;
         rotationSession = this;
         locked = true;
         chosen = false;
         rotatingItems = new ItemStack[9];
         PixelGifts.i.getSessions().add(this);
         for(int i = 0; i < 9; i++){
             rotatingItems[i] = PixelGifts.i.getItems().next();
         }

         toRotate = Bukkit.createInventory(null, 9, PixelGifts.i.getSpininv());
         for(int i = 0; i< 9; i++){
             toRotate.setItem(i,PixelGifts.i.getUnchosen());
         }
         p.openInventory(toRotate);
     }

    public void rotate(int slotChosen){
        chosen = true;
        if(slotChosen > 8) return;
        chosenSlot = slotChosen;
        finalItem = rotatingItems[slotChosen];
        for(int i = 0; i<9; i++){
            if(i != slotChosen){
                toRotate.setItem(i,PixelGifts.i.getChosenother());
            }
        }
        toRotate.setItem(slotChosen, PixelGifts.i.getChosen());
        task = Bukkit.getScheduler().scheduleSyncRepeatingTask(PixelGifts.i, new Runnable() {
            @Override
            public void run() {
                if(counter == -1){
                    return;
                }
                if(counter == 9){
                    end(slotChosen,task);
                    counter = -1;
                }else{
                    toRotate.setItem(counter,rotatingItems[counter]);
                    counter++;
                }
            }
        },10,8);
    }

    public void end(int chosen, int task){
        Bukkit.getScheduler().scheduleSyncDelayedTask(PixelGifts.i, new Runnable() {
            @Override
            public void run() {
                for(int i = 0; i < 9; i++){
                    if(i != chosen) toRotate.setItem(i,null);
                }
                p.playSound(p.getLocation(), Sound.LEVEL_UP, 1,1);
                locked = false;
                PixelGifts.i.getItemCommands().get(finalItem).forEach(s -> Bukkit.dispatchCommand(Bukkit.getConsoleSender(),s));
                Bukkit.getScheduler().cancelTask(task);
            }
        },20);
    }

    public Player getP() {
        return p;
    }

    public Inventory getToRotate() {
        return toRotate;
    }

    public ItemStack getFinalItem() {
        return finalItem;
    }

    public int getTask() {
        return task;
    }

    public int getChosenSlot() {
        return chosenSlot;
    }
}
